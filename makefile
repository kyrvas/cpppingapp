.PHONY: all test clean

CC=gcc
CFLAGS= -static-libgcc -static-libstdc++
DEPS=
OBJ=ping.o 
RM = rm

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

ping : $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

test:
	sudo ./ping
	
clean:
	$(RM) *.o ping