#include <arpa/inet.h> //inet_pton
#include <cstring>     //memset
#include <errno.h>		//strerror
#include <stdio.h>	//printf
#include <sys/socket.h>
#include <unistd.h> //close
#include <stdlib.h>	//exit
#include <netinet/in.h> //raw

#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <sys/time.h>//time
 
#include <netdb.h>

const char* host = "www.google.com";//127.0.0.1"; //localhost //8.8.8.8

#define	BUFSIZE		1500
			/* globals */
//char	 recvbuf[BUFSIZE];
char sendbuf[BUFSIZE];
pid_t pid; /* our PID */ 
int	nsent = 0; /* add 1 for each sendto() */
int datalen = 60;//56; /* #bytes of data, following ICMP header */

unsigned short in_cksum(unsigned short *addr, int len);//checksum from inet

struct sockaddr  *sasend;	/* sockaddr{} for send, from getaddrinfo */
socklen_t	    salen;		/* length of sockaddr{}s */

int sendIcmp(const char * ip_, const char * msg_)
{

	return 0;
}
 
int main()
{
	int sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if(sock < 0) {
	// error in socket creation
	// exit
	}
    pid = getpid();

//-------address
	int n;
	struct addrinfo	hints, *res;
	//bzero(&hints, sizeof(struct addrinfo));
    memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = AI_CANONNAME ;	/* always return canonical name */
	hints.ai_family = AF_INET;		/* AF_UNSPEC, AF_INET, AF_INET6, etc. */
	hints.ai_socktype = 0;	/* 0, SOCK_STREAM, SOCK_DGRAM, etc. */
//	if ( (n = getaddrinfo(host, NULL, &hints, &sasend)) != 0){
   
    if ( (n = getaddrinfo(host, "http", &hints, &res)) != 0){
    
    // error in address
    // exit
//    return 1;
    int err = errno;
	printf("addr error=%d: %s \n", err, strerror(err));
//    return 1;
    }
    sasend = res->ai_addr;
    salen = res->ai_addrlen;

    int	len;
	struct icmp	*icmp;
	icmp = (struct icmp *) sendbuf;
	icmp->icmp_type = ICMP_ECHO;
	icmp->icmp_code = 0;
	icmp->icmp_id = pid;
	icmp->icmp_seq = nsent++;



	len = 8 + datalen;		/* checksum ICMP header and data */
    const int bs  = 1500;
    char buff[bs];

    char ch = 'a';
    char z_ = 'z' + 1;
    for (int i = 0; i< bs; ++i)
    {
       buff[i] = ch;
       ch = int(ch) + 1;
       if (ch == z_) ch = 'a';
    }

        
    for (int i = 0; i< len; ++i)
    {
        ((char*)icmp->icmp_data)[i] = buff[i];
    }

//	gettimeofday((struct timeval *) icmp->icmp_data, NULL);
    
	icmp->icmp_cksum = 0;
	icmp->icmp_cksum = in_cksum((u_short *) icmp, len);
//    for(int i=0; i< 10; ++i)
	sendto(sock, sendbuf, len, 0, sasend, salen);    
	exit(0);
}

unsigned short in_cksum(unsigned short *addr, int len)
{
	int				nleft = len;
	int				sum = 0;
	unsigned short	*w = addr;
	unsigned short	answer = 0;

	/*
	 * Our algorithm is simple, using a 32 bit accumulator (sum), we add
	 * sequential 16 bit words to it, and at the end, fold back all the
	 * carry bits from the top 16 bits into the lower 16 bits.
	 */
	while (nleft > 1)  {
		sum += *w++;
		nleft -= 2;
	}

		/* 4mop up an odd byte, if necessary */
	if (nleft == 1) {
		*(unsigned char *)(&answer) = *(unsigned char *)w ;
		sum += answer;
	}

		/* 4add back carry outs from top 16 bits to low 16 bits */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */
	return(answer);
}
